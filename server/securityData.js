const fetch = require('node-fetch');

let profileData = (ticker) => {

    let promise = new Promise(function (resolve, reject) {

        // let url = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' + ticker + '?modules=assetProfile%2CsummaryDetail%2CesgScores%2CincomeStatementHistorysummaryDetail%2CesgScores%2CincomeStatementHistory%2CbalanceSheetHistory%2CcashflowStatementHistory%2CdefaultKeyStatistics%2CfinancialData%2CinstitutionOwnership%2Cearnings%2CearningsHistory%2CearningsTrend';
        let url = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' + ticker + '?modules=assetProfile';
        // console.log(url);

        fetch(url)
            .then(res => res.text())
            .then(body => {
                resolve(body);
            })
            .catch(err => reject(err));
    });

    return promise;
}

let incomeStatement = (ticker) => {

    let promise = new Promise(function (resolve, reject) {

        let url = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' + ticker + '?modules=incomeStatementHistory';
        // console.log(url);

        fetch(url)
            .then(res => res.text())
            .then(body => {
                resolve(body);
            })
            .catch(err => reject(err));
    });

    return promise;
}

let incomeStatementQuarterly = (ticker) => {

    let promise = new Promise(function (resolve, reject) {

        let url = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' + ticker + '?modules=incomeStatementHistoryQuarterly';
        // console.log(url);

        fetch(url)
            .then(res => res.text())
            .then(body => {
                resolve(body);
            })
            .catch(err => reject(err));
    });

    return promise;
}

let balanceSheet = (ticker) => {

    let promise = new Promise(function (resolve, reject) {

        let url = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' + ticker + '?modules=balanceSheetHistory';
        // console.log(url);

        fetch(url)
            .then(res => res.text())
            .then(body => {
                resolve(body);
            })
            .catch(err => reject(err));
    });

    return promise;
}

let balanceSheetQuarterly = (ticker) => {

    let promise = new Promise(function (resolve, reject) {

        let url = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' + ticker + '?modules=balanceSheetHistoryQuarterly';
        // console.log(url);

        fetch(url)
            .then(res => res.text())
            .then(body => {
                resolve(body);
            })
            .catch(err => reject(err));
    });

    return promise;
}

let cashflowStatement = (ticker) => {

    let promise = new Promise(function (resolve, reject) {

        let url = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' + ticker + '?modules=cashflowStatementHistory';
        // console.log(url);

        fetch(url)
            .then(res => res.text())
            .then(body => {
                resolve(body);
            })
            .catch(err => reject(err));
    });

    return promise;
}

let cashflowStatementQuarterly = (ticker) => {

    let promise = new Promise(function (resolve, reject) {

        let url = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' + ticker + '?modules=cashflowStatementHistoryQuarterly';
        // console.log(url);

        fetch(url)
            .then(res => res.text())
            .then(body => {
                resolve(body);
            })
            .catch(err => reject(err));
    });

    return promise;
}

let financialData = (ticker) => {

    let promise = new Promise(function (resolve, reject) {

        let url = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' + ticker + '?modules=financialData';
        // console.log(url);

        fetch(url)
            .then(res => res.text())
            .then(body => {
                resolve(body);
            })
            .catch(err => reject(err));
    });

    return promise;
}

let intrinsicData = (ticker) => {

    let promise = new Promise(function (resolve, reject) {

        let url = 'https://query2.finance.yahoo.com/v10/finance/quoteSummary/' + ticker + '?modules=defaultKeyStatistics%2CearningsTrend%2CbalanceSheetHistoryQuarterly%2CcashflowStatementHistoryQuarterly';
        console.log(url);

        fetch(url)
            .then(res => res.text())
            .then(body => {

                let payload = JSON.parse(body);
                let balanceSheetStatements = payload.quoteSummary.result[0].balanceSheetHistoryQuarterly.balanceSheetStatements;
                let cashflowStatements = payload.quoteSummary.result[0].cashflowStatementHistoryQuarterly.cashflowStatements;
                let defaultKeyStatistics = payload.quoteSummary.result[0].defaultKeyStatistics;
                let earningsTrend = payload.quoteSummary.result[0].earningsTrend.trend;

                let trend = earningsTrend.filter(t => t.period === '+5y')[0];
                let growth = trend.growth.raw;
                let numberofSharesOutstanding = defaultKeyStatistics.sharesOutstanding.raw;
                let beta = defaultKeyStatistics.beta.raw;

                let latest = -1;
                let totalCashAndShortTermInvestments = 0;
                let totalDebt = 0;

                for (let i = 0; i < balanceSheetStatements.length; i++) {
                    let balanceSheetStatement = balanceSheetStatements[i];
                    if (balanceSheetStatement.endDate.raw > latest) {
                        latest = balanceSheetStatement.endDate.raw;

                        if (balanceSheetStatement.cash) {
                            totalCashAndShortTermInvestments += balanceSheetStatement.cash.raw;
                        }

                        if (balanceSheetStatement.shortTermInvestments) {
                            totalCashAndShortTermInvestments += balanceSheetStatement.shortTermInvestments.raw;
                        }

                        if (balanceSheetStatement.shortLongTermDebt) {
                            totalDebt += balanceSheetStatement.shortLongTermDebt.raw;
                        }

                        if (balanceSheetStatement.longTermDebt) {
                            totalDebt += balanceSheetStatement.longTermDebt.raw;
                        }
                    }
                }

                let totalCashFromOperatingActivities = 0;
                let totalCapitalExpenditures = 0;
                for (let i = 0; i < cashflowStatements.length; i++) {
                    let cashflowStatement = cashflowStatements[i];
                    if (cashflowStatement.totalCashFromOperatingActivities) {
                        totalCashFromOperatingActivities += cashflowStatement.totalCashFromOperatingActivities.raw;
                    }

                    if (cashflowStatement.capitalExpenditures) {
                        totalCapitalExpenditures += cashflowStatement.capitalExpenditures.raw;
                    }
                }

                resolve(JSON.stringify({
                    totalCashFromOperatingActivities: totalCashFromOperatingActivities,
                    freeCashFlow: (totalCashFromOperatingActivities + totalCapitalExpenditures),
                    growthRate: growth,
                    totalCashAndShortTermInvestments: totalCashAndShortTermInvestments,
                    totalDebt: totalDebt,
                    numberofSharesOutstanding: numberofSharesOutstanding,
                    beta: beta
                }));
            })
            .catch(err => reject(err));
    });

    return promise;
}

module.exports = {
    balanceSheet,
    balanceSheetQuarterly,
    cashflowStatement,
    cashflowStatementQuarterly,
    financialData,
    incomeStatement,
    incomeStatementQuarterly,
    intrinsicData,
    profileData
}
