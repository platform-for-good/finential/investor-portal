const fetch = require('node-fetch');

let historicalData = (ticker, startYear, startMonth, startDate, endYear, endMonth, endDate, interval) => {

    let promise = new Promise(function (resolve, reject) {

        const start = Math.round((new Date(startYear, startMonth - 1, startDate)).getTime() / 1000);
        const end = Math.round((new Date(endYear, endMonth - 1, endDate)).getTime() / 1000);

        let url = 'https://query1.finance.yahoo.com/v7/finance/download/' + ticker + '?period1=' + start + '&period2=' + end + '&interval=' + interval + '&events=history&includeAdjustedClose=false';
        console.log(url);

        fetch(url)
            .then(res => res.text())
            .then(body => {
                resolve(body);
            })
            .catch(err => reject(err));
    });

    return promise;
}

module.exports = {
    historicalData
}
