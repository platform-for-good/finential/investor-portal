const EventEmitter = require('events');
class JobEmitter extends EventEmitter { }
const emitter = new JobEmitter();
const { execFile } = require('child_process');
const HashMap = require('hashmap');
const path = require('path');

const spark = "../../processing/spark/";
const venvActivate = "/venv/Scripts/activate.bat";
const venvDeactivate = "/venv/Scripts/deactivate.bat";

let map = new HashMap();

let run = (data) => {
    let payload = JSON.parse(data);
    let job = payload.job;

    console.log('Running spark job for: ' + job + " with " + payload.args);

    let batchPath = path.resolve(spark + job + "/run.bat");
    let activateVenv = path.resolve(spark + job + venvActivate);
    let deactivateVenv = path.resolve(spark + job + venvDeactivate);
    let pythonFilePath = path.resolve(spark + job + "/spark_" + job + ".py");
    let sourcePath = path.resolve(payload.input) + "\\";
    let destinationPath = path.resolve(payload.output + job) + "\\";

    let args;
    switch (job) {
        case "intrinsic_value":
            {
                args = [activateVenv, pythonFilePath, payload.args[0], sourcePath, destinationPath, deactivateVenv];
            }
            break;
        case "pivot_point":
            {
                args = [activateVenv, pythonFilePath, payload.args[0], sourcePath, destinationPath, deactivateVenv];
            }
            break;
        case "rsi":
            {
                args = [activateVenv, pythonFilePath, payload.args[0], payload.args[1], payload.args[2], payload.args[3], sourcePath, destinationPath, deactivateVenv];
            }
            break;
        case "trend":
            {
                args = [activateVenv, pythonFilePath, payload.args[0], payload.args[1], sourcePath, destinationPath, deactivateVenv];
            }
            break;
        case "support_resistance":
            {
                args = [activateVenv, pythonFilePath, payload.args[0], payload.args[1], sourcePath, destinationPath, deactivateVenv];
            }
            break;
    }

    console.log("spark-sumit " + pythonFilePath + " with args " + args);

    let childProcess = execFile(batchPath, args, { maxBuffer: Infinity }, function (err, stdout, stderr) {
        if (err) {
            console.error("Failed to start %s. Error: %s", batchPath, err);
        }

        // console.log(stdout);
        console.log(stderr);
    });

    map.set(childProcess.pid, { id: payload.id, job: job, ticker: payload.args[0] });

    childProcess.on("close", function (code) {

        let process = map.get(childProcess.pid);
        console.log(process.job + " (" + process.id + ") for " + process.ticker + " is completed.");
        map.delete(childProcess.pid);

        emitter.emit("spark-job-completed", JSON.stringify({ id: process.id, job: process.job, ticker: process.ticker, code: code }));
        // io.emit('spark-job-completed', JSON.stringify({ id: process.id, job: process.job, ticker: process.ticker }));

    }).on("exit", function (code) {
        console.log(childProcess.pid + " exited");
    });
}

module.exports = {
    emitter,
    run
}