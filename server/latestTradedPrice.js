const fetch = require('node-fetch');

let latestTradedPrice = (ticker) => {

    let promise = new Promise(function (resolve, reject) {

        let url = "https://query2.finance.yahoo.com/v10/finance/quoteSummary/" + ticker + "?modules=price";
        // console.log(url);
        
        fetch(url)
            .then(res => res.text())
            .then(body => {
                let payload = JSON.parse(body);
                resolve(payload);
            })
            .catch(err => reject(err));
    });

    return promise;
}

module.exports = {
    latestTradedPrice
}
