const Client = require('kafka-node').KafkaClient;
const Consumer = require('kafka-node').Consumer;
const Offset = require('kafka-node').Offset;

const kafkaHost = 'localhost:9092';

let subscribe = (currentTopics) => {
    console.log("Total: " + currentTopics.length);
    const client = new Client({ kafkaHost });
    let topics = [];
    for (let i = 0; i < currentTopics.length; i++) {
        topics.push({ topic: currentTopics[i], partition: 0 });
    }

    const options = { autoCommit: false, fetchMaxWaitMs: 1000, fetchMaxBytes: 1024 * 1024 };

    const consumer = new Consumer(client, topics, options);
    consumer.on('error', function (err) {
        console.log('error', err);
    });

    client.refreshMetadata(
        currentTopics,
        (err) => {
            const offset = new Offset(client);

            if (err) {
                throw err;
            }

            consumer.on('message', function (message) {
                // do something useful with message
                console.log(message);
            });

            /*
             * If consumer get `offsetOutOfRange` event, fetch data from the smallest(oldest) offset
             */
            consumer.on(
                'offsetOutOfRange',
                (topic) => {
                    offset.fetch([topic], function (err, offsets) {
                        if (err) {
                            return console.error(err);
                        }
                        const min = Math.min.apply(null, offsets[topic.topic][topic.partition]);
                        consumer.setOffset(topic.topic, topic.partition, min);
                    });
                }
            );
        }
    );
}

let topics = ["MMM", "AXP", "AMGN", "AAPL", "BA", "CAT", "CVX", "CSCO", "KO", "DOW", "GS", "HD", "HON", "IBM", "INTC", "JNJ", "JPM", "MCD", "MRK", "MSFT", "NKE", "PG", "CRM", "TRV", "UNH", "VZ", "V", "WBA", "WMT", "DIS"];
subscribe(topics);