const fs = require('fs');
const moment = require('moment');
// const WebSocket = require('ws');

const jsonServer = require('json-server');
const router = jsonServer.router('db.json');
const middlewares = jsonServer.defaults({ static: '../../storage' });
const server = jsonServer.create();

const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http, {
    cors: {
        origins: ['http://localhost:4200']
    }
});

const historicalData = require('./historicalData').historicalData;
const latestTradedPrice = require('./latestTradedPrice').latestTradedPrice;
const newsFeed = require('./newsFeed').newsFeed;
const financialData = require('./securityData').financialData;
const intrinsicData = require('./securityData').intrinsicData;
const incomeStatement = require('./securityData').incomeStatement;
const incomeStatementQuarterly = require('./securityData').incomeStatementQuarterly;
const balanceSheet = require('./securityData').balanceSheet;
const balanceSheetQuarterly = require('./securityData').balanceSheetQuarterly;
const cashflowStatement = require('./securityData').cashflowStatement;
const cashflowStatementQuarterly = require('./securityData').cashflowStatementQuarterly;
const profileData = require('./securityData').profileData;
const sparkJob = require('./spark');
const producer = require('./producer');

const stage = "../../storage/s1/";
const processedFeatureStage = "../../storage/s2/";
const database = "./db.json";

// const wss = new WebSocket.Server({ host: "localhost", port: 3010 });

let payload = {
    securities: []
}
let tickers = [];
let init = (stockIndex, file, tickerIndex, nameIndex) => {

    if (!fs.existsSync(stage)) {
        fs.mkdirSync(stage, { recursive: true });
    }

    let directory = stage + 'securities/1d';

    if (!fs.existsSync(directory)) {
        fs.mkdirSync(directory, { recursive: true });
    }

    directory = stage + 'profile';
    if (!fs.existsSync(directory)) {
        fs.mkdirSync(directory, { recursive: true });
    }

    directory = stage + 'financial';
    if (!fs.existsSync(directory)) {
        fs.mkdirSync(directory, { recursive: true });
    }

    directory = stage + 'news';
    if (!fs.existsSync(directory)) {
        fs.mkdirSync(directory, { recursive: true });
    }

    fs.readFile(file, 'utf8', (err, data) => {
        let lines = data.split('\n');
        for (let i = 1; i < lines.length; i++) {
            let line = lines[i];
            let tokens = line.split(',');
            let ticker = tokens[tickerIndex];
            tickers.push(ticker);

            payload.securities.push({
                id: i,
                name: tokens[nameIndex],
                ticker: ticker
            })
        }

        fs.writeFileSync(database, JSON.stringify(payload));

        let today = new Date();
        let endYear = today.getFullYear();
        let endMonth = today.getMonth() + 1;
        let endDate = today.getDate();
        let startYear = endYear - 100;//endYear - 100; //download from now to previous 100 years
        let startMonth = 1;
        let startDate = 1;

        for (let i = 0; i < tickers.length; i++) {
            let ticker = tickers[i];

            // console.log("Downloading data for " + ticker);

            historicalData(ticker, startYear, startMonth, startDate, endYear, endMonth, endDate, '1d').then(data => {
                let error = false;
                try {
                    let payload = JSON.parse(data);
                    if (payload.finance.error) {
                        error = true;
                    }
                } catch {

                }

                if (!error) {
                    console.log("Downloading historical data for " + ticker);
                    fs.writeFileSync(stage + 'securities/1d/' + ticker + ".csv", data);
                } else {
                    console.log("Error downloading historical data for " + ticker + " Error: " + data);
                }

            }).catch(error => {
                console.error(error);
            });

            profileData(ticker).then(data => {
                fs.writeFileSync(stage + 'profile/' + ticker + ".json", data);
            }).catch(error => {
                console.error(error);
            });

            incomeStatement(ticker).then(data => {
                fs.writeFileSync(stage + 'financial/' + ticker + "_income.json", data);
            }).catch(error => {
                console.error(error);
            });

            incomeStatementQuarterly(ticker).then(data => {
                fs.writeFileSync(stage + 'financial/' + ticker + "_incomeQtr.json", data);
            }).catch(error => {
                console.error(error);
            });

            balanceSheet(ticker).then(data => {
                fs.writeFileSync(stage + 'financial/' + ticker + "_balance.json", data);
            }).catch(error => {
                console.error(error);
            });

            balanceSheetQuarterly(ticker).then(data => {
                fs.writeFileSync(stage + 'financial/' + ticker + "_balanceQtr.json", data);
            }).catch(error => {
                console.error(error);
            });

            cashflowStatement(ticker).then(data => {
                fs.writeFileSync(stage + 'financial/' + ticker + "_cashflow.json", data);
            }).catch(error => {
                console.error(error);
            });

            cashflowStatementQuarterly(ticker).then(data => {
                fs.writeFileSync(stage + 'financial/' + ticker + "_cashflowQtr.json", data);
            }).catch(error => {
                console.error(error);
            });

            financialData(ticker).then(data => {
                fs.writeFileSync(stage + 'financial/' + ticker + "_financial.json", data);
            }).catch(error => {
                console.error(error);
            });

            intrinsicData(ticker).then(data => {
                fs.writeFileSync(stage + 'financial/' + ticker + "_intrinsic.json", data);
            }).catch(error => {
                console.error(error);
            });

            downloadNews(ticker);
            setInterval(() => {
                downloadNews(ticker);
                //every day
            }, 1 * 24 * 60 * 1000);
        }

        //every 60s
        fetch(60);
        batch(stockIndex);

        io.on('connection', (socket) => {
            //do a broadcast once the portal is connected
            broadcastLatestPrice();

            socket.emit("connected", true);
            socket.on('run-spark', (data) => {

                sparkJob.run(data);
                sparkJob.emitter.on("spark-job-completed", (result) => {
                    io.emit('spark-job-completed', result);
                });

            });
        });
    });
}

let deleteFolderRecursive = function (path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file, index) {
            let currentPath = path + "/" + file;
            if (fs.lstatSync(currentPath).isDirectory()) { // recurse
                deleteFolderRecursive(currentPath);

            } else { // delete file
                fs.unlinkSync(currentPath);
            }
        });

        fs.rmdirSync(path);
    }
};

let downloadNews = (ticker) => {
    newsFeed(ticker).then(data => {
        fs.writeFileSync(stage + 'news/' + ticker + moment().format("YYYYMMMDDHHmm") + ".xml", data);
    });
}

let broadcastLatestPrice = () => {
    for (let i = 0; i < tickers.length; i++) {
        let ticker = tickers[i];

        latestTradedPrice(ticker).then(data => {

            let stockInformation = data.quoteSummary.result[0].price;
            postMarket = stockInformation.marketState === "POST" || stockInformation.marketState === "POSTPOST";
            let payload = {
                name: stockInformation.shortName,
                ticker: stockInformation.symbol,
                date: moment().format("YYYY-MMM-DD HH:mm:ss"),
                preMarketChangePercent: stockInformation.preMarketChangePercent ? stockInformation.preMarketChangePercent.raw : 0,
                preMarketChange: stockInformation.preMarketChange ? stockInformation.preMarketChange.raw : 0,
                preMarketPrice: stockInformation.preMarketPrice ? stockInformation.preMarketPrice.raw : 0,
                regularMarketPrice: stockInformation.regularMarketPrice ? stockInformation.regularMarketPrice.raw : 0,
                regularMarketChange: stockInformation.regularMarketChange ? stockInformation.regularMarketChange.raw : 0,
                regularMarketChangePercent: stockInformation.regularMarketChangePercent ? stockInformation.regularMarketChangePercent.raw : 0,
                postMarketChangePercent: stockInformation.postMarketChangePercent ? stockInformation.postMarketChangePercent.raw : 0,
                postMarketPrice: stockInformation.postMarketPrice ? stockInformation.postMarketPrice.raw : 0,
                postMarketChange: stockInformation.postMarketChange ? stockInformation.postMarketChange.raw : 0,
                currency: stockInformation.currency,
                exchangeName: stockInformation.exchangeName,
                preMarket: stockInformation.marketState === "PRE", //|| stockInformation.marketState === "PREPRE",
                regularMarket: stockInformation.marketState === "REGULAR",
                postMarket: stockInformation.marketState === "POST", //|| stockInformation.marketState === "POSTPOST",
                regularMarketPreviousClose: stockInformation.regularMarketPreviousClose ? stockInformation.regularMarketPreviousClose.raw : 0,
                regularMarketDayHigh: stockInformation.regularMarketDayHigh ? stockInformation.regularMarketDayHigh.raw : 0,
                regularMarketDayLow: stockInformation.regularMarketDayLow ? stockInformation.regularMarketDayLow.raw : 0,
                regularMarketVolume: stockInformation.regularMarketVolume ? stockInformation.regularMarketVolume.raw : 0,
                marketCap: stockInformation.marketCap.raw
            }

            // console.log(moment().format("YYYY-MMM-DD HH:mm:ss") + " " + ticker + " " + JSON.stringify(payload));
            io.emit('stock-info', JSON.stringify(payload));

            // producer.publish(ticker,
            //     JSON.stringify(
            //         {
            //             ticker: payload.ticker,
            //             date: payload.date,
            //             pre: payload.preMarketPrice,
            //             price: payload.regularMarketPrice,
            //             post: payload.postMarketPrice
            //         }));

            // wss.on('connection', ws => {
            //     wss.clients.forEach(
            //         (client) => {
            //             client.send(JSON.stringify(payload));
            //         }
            //     );
            // });
        }).catch(err => console.error(err));
    }
}

let batch = (stockIndex) => {
    let ticker = stockIndex;
    let jobs = [
        {
            id: Date.now(),
            input: stage + 'financial',
            output: processedFeatureStage,
            job: 'intrinsic_value',
            args: [ticker]
        },
        {
            id: Date.now(),
            input: stage + 'securities/1d',
            output: processedFeatureStage,
            job: 'pivot_point',
            args: [ticker]
        },
        {
            id: Date.now(),
            input: stage + 'securities/1d',
            output: processedFeatureStage,
            job: 'rsi',
            args: [ticker, 14, 80, 20]
        },
        {
            id: Date.now(),
            input: stage + 'securities/1d',
            output: processedFeatureStage,
            job: 'trend',
            args: [ticker, -60]
        },
        {
            id: Date.now(),
            input: stage + 'securities/1d',
            output: processedFeatureStage,
            job: 'support_resistance',
            args: [ticker, -60]
        },
    ];

    for (let i = 0; i < jobs.length; i++) {
        let job = JSON.stringify(jobs[i]);
        sparkJob.run(job);
    }

    sparkJob.emitter.on("spark-job-completed", (result) => {
        let payload = JSON.parse(result);
        console.log(payload.job + " is completed. Result: " + result);
    });
}

let fetch = async (interval) => {

    //broadcast at an interval
    setInterval(() => {
        broadcastLatestPrice();
    }, interval * 1000);
}

init('DJIX', './raw/DJIA.csv', 2, 0);
// init('SPX', './raw/SPX.csv', 0, 1);

server.use(middlewares);
server.use(router);
server.listen(3000, () => {
    console.log('JSON Server is running');
});

http.listen(3001, () => {
    console.log('listening on *:3001');
});