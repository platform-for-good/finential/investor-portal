const fetch = require('node-fetch');

let newsFeed = (ticker) => {

    let promise = new Promise(function (resolve, reject) {

        let url = 'https://feeds.finance.yahoo.com/rss/2.0/headline?s=' + ticker + '&region=US&lang=en-US';
        // console.log(url);

        fetch(url)
            .then(res => res.text())
            .then(body => {
                resolve(body);
            })
            .catch(err => console.error(err));
    });

    return promise;
}

module.exports = {
    newsFeed
}
