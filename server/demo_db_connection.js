// https://www.w3schools.com/nodejs/nodejs_mysql.asp
const mysql = require('mysql');

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "1Q2w#e4R"
});

connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});