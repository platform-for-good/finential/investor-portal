const Client = require('kafka-node').KafkaClient;
const Producer = require('kafka-node').Producer;

const kafkaHost = 'localhost:9092';

let publish = (topic, message) =>{
    // The client connects to a Kafka broker
    const client = new Client({ kafkaHost });
    // The producer handles publishing messages over a topic
    const producer = new Producer(client);

    producer.on('ready',()=>{

        // Update metadata for the topic we'd like to publish to
        client.refreshMetadata(
            [topic],
            (err) => {
                if (err) {
                    throw err;
                }

                console.log(`Sending message to ${topic}: ${message}`);
                producer.send(
                    [{ topic, messages: [message] }],
                    (err, result) => {
                        console.log(err || result);
                        // process.exit();
                    }
                );
            }
        );

    }).on('error',(err)=>{
        console.log('error', err);
    });
}

module.exports = {
    publish
}
// publish("test", "hello world");
// publish("abc", "hello neaven");