# Investor Portal

Conceptualize systems of record, engagement and insights that support the data warehouse and data lake architecture to help investors make wise investment decisions.