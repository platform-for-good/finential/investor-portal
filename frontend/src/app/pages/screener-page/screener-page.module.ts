import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScreenerPageRoutingModule } from './screener-page-routing.module';
import { ScreenerPageComponent } from './screener-page.component';


@NgModule({
  declarations: [ScreenerPageComponent],
  imports: [
    CommonModule,
    ScreenerPageRoutingModule
  ]
})
export class ScreenerPageModule { }
