import { ScreenerPageComponent } from './screener-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [ {path:'',component:ScreenerPageComponent,data:{shouldReuse:true,key:'screener'}},  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScreenerPageRoutingModule { }
