import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SocketioService } from 'src/app/lambda/socketio.service';
import { Stock } from 'src/app/models/stock';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit, OnDestroy {

  @Input() ticker: string;
  @Input() stockInfo: Stock;
  subscription: Subscription;
  
  constructor(private socketioService: SocketioService) { 
    // this.socketioService.setupSocketConnection();
    // this.subscription = this.socketioService.stock.subscribe(payload => {
    //   // console.log(payload);
    //   if (payload.ticker === this.ticker) {
    //     this.stockInfo = payload;
    //   }
    // });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    // this.webSocketService.closeConnection();
    this.subscription.unsubscribe();
  }

}
