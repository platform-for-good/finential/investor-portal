import { Component, Input, OnInit } from '@angular/core';
import { FinancialData } from 'src/app/models/financial-data';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  @Input() ticker: string;
  @Input() stockStatistics: FinancialData;

  constructor() { }

  ngOnInit() {
  }

}
