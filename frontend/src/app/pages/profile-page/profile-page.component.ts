import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ECharts } from 'echarts';
import { Subscription } from 'rxjs';
import { SecuritiesService } from 'src/app/lambda/securities.service';
import { SocketioService } from 'src/app/lambda/socketio.service';
import { FinancialData } from 'src/app/models/financial-data';
import { Profile } from 'src/app/models/profile';
import { Stock } from 'src/app/models/stock';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {

  echartsInstance: ECharts;
  options: any;
  name: string;
  ticker: string;
  stockInfo: Stock;
  prices: any;
  details: Profile;
  stockStatistics: FinancialData;
  subscription: Subscription;

  constructor(private securitiesService: SecuritiesService, private socketioService: SocketioService, private formBuilder: FormBuilder) {
    this.ticker = history.state.data.ticker;
    this.name = history.state.data.name;

    socketioService.setupSocketConnection();
    this.subscription = socketioService.stock.subscribe(payload => {
      if (payload && payload.ticker === this.ticker) {
        this.stockInfo = payload;
      }
    });
  }

  ngOnInit() {
    if (this.ticker) {
      this.displayStockChart();
      this.populate();
    }
  }

  ngDoCheck() {
    if (history.state.data.ticker !== this.ticker) {
      this.ticker = history.state.data.ticker;
      this.name = history.state.data.name;

      if (this.ticker) {
        this.displayStockChart();
        this.populate();
      }
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  displayStockChart(): void {
    this.securitiesService.getAllSecurityPrice(this.ticker).then(prices => {

      const dates = [];
      const volumes = [];
      const values = [];
      for (let i = 0; i < prices.length; i++) {
        dates.push(prices[i].splice(0, 1)[0]);
        volumes.push(prices[i].splice(4, 1)[0]);
        values.push(prices[i]);
      }

      let upColor = '#00da3c';
      let downColor = '#ec0000';
      this.options = {
        animation: false,
        legend: {
          top: 30,
          left: 'center',
          data: [this.ticker]
        },
        title: {
          text: this.ticker,
          left: 'center',
        },
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'cross'
          },
          borderWidth: 1,
          borderColor: '#ccc',
          padding: 10,
          textStyle: {
            color: '#000'
          },
          // position: function (pos, params, el, elRect, size) {
          //   var obj = { top: 10 };
          //   obj[['left', 'right'][+(pos[0] < size.viewSize[0] / 2)]] = 30;
          //   return obj;
          // }
          // extraCssText: 'width: 170px'
        },
        axisPointer: {
          link: { xAxisIndex: 'all' },
          label: {
            backgroundColor: '#777'
          }
        },
        // toolbox: {
        //   feature: {
        //     dataZoom: {
        //       yAxisIndex: false
        //     },
        //     brush: {
        //       type: ['lineX', 'clear']
        //     }
        //   }
        // },
        // brush: {
        //   xAxisIndex: 'all',
        //   brushLink: 'all',
        //   outOfBrush: {
        //     colorAlpha: 0.1
        //   }
        // },
        visualMap: {
          show: false,
          seriesIndex: 5,
          dimension: 2,
          pieces: [{
            value: 1,
            color: downColor
          }, {
            value: -1,
            color: upColor
          }]
        },
        grid: [
          {
            left: '10%',
            right: '8%',
            height: '50%'
          },
          {
            left: '10%',
            right: '8%',
            top: '63%',
            height: '16%'
          }
        ],
        xAxis: [
          {
            type: 'category',
            data: dates,
            scale: true,
            boundaryGap: false,
            axisLine: { onZero: false },
            splitLine: { show: true },
            splitNumber: 20,
            min: 'dataMin',
            max: 'dataMax',
            axisPointer: {
              z: 100
            }
          },
          {
            type: 'category',
            gridIndex: 1,
            data: dates,
            scale: true,
            boundaryGap: false,
            axisLine: { onZero: false },
            axisTick: { show: false },
            splitLine: { show: false },
            axisLabel: { show: false },
            splitNumber: 20,
            min: 'dataMin',
            max: 'dataMax'
          }
        ],
        yAxis: [
          {
            scale: true,
            splitArea: {
              show: true
            }
          },
          {
            scale: true,
            gridIndex: 1,
            splitNumber: 2,
            axisLabel: { show: false },
            axisLine: { show: false },
            axisTick: { show: false },
            splitLine: { show: false }
          }
        ],
        dataZoom: [
          {
            type: 'inside',
            xAxisIndex: [0, 1],
            start: 0,
            end: 100
          },
          {
            show: true,
            xAxisIndex: [0, 1],
            type: 'slider',
            top: '85%',
            start: 0,
            end: 100
          }
        ],
        series: [
          {
            name: this.ticker,
            type: 'candlestick',
            data: values,
            itemStyle: {
              color: upColor,
              color0: downColor,
              borderColor: null,
              borderColor0: null
            }
          },
          {
            name: 'Volume',
            type: 'bar',
            xAxisIndex: 1,
            yAxisIndex: 1,
            data: volumes
          }
        ]
      }

    });
  }

  public onChartInit(echarts): void {
    this.echartsInstance = echarts;
  }

  public filter(option: string): void {

    if (this.echartsInstance) {
      this.securitiesService.getAllSecurityPrice(this.ticker).then(prices => {

        let remaining = prices;
        switch (option) {
          case '1D':
            remaining = prices.slice(prices.length - 1);
            break;
          case '5D':
            remaining = prices.slice(prices.length - 5);
            break;
          case '1M':
            remaining = prices.slice(prices.length - 28);
            break
          case '6M':
            remaining = prices.slice(prices.length - 180);
            break;
          case '1Y':
            remaining = prices.slice(prices.length - 365);
            break;
          case '5Y':
            remaining = prices.slice(prices.length - 1825);
            break;
          default:
            break;
        }

        const dates = [];
        const volumes = [];
        const values = [];

        for (let i = 0; i < remaining.length; i++) {
          dates.push(remaining[i].splice(0, 1)[0]);
          volumes.push(remaining[i].splice(4, 1)[0]);
          values.push(remaining[i]);
        }

        this.options.xAxis[0].data = dates;
        this.options.xAxis[1].data = dates;
        this.options.series[0].data = values;
        this.options.series[1].data = volumes;
        this.echartsInstance.setOption(this.options, true, false);

      });
    }
  }

  public populate(): void {

    this.securitiesService.getSecurityProfile(this.ticker).then(profile => {
      this.details = profile;
    });

    this.securitiesService.getSecurityFinancialData(this.ticker).then(statistics => {
      this.stockStatistics = statistics;
    });
  }
}

