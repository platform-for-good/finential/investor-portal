import { Component, Input, OnInit } from '@angular/core';
import { ECharts } from 'echarts';
import { SecuritiesService } from 'src/app/lambda/securities.service';

@Component({
  selector: 'app-financial',
  templateUrl: './financial.component.html',
  styleUrls: ['./financial.component.scss']
})
export class FinancialComponent implements OnInit {

  @Input() ticker: string;

  // echartsInstance: ECharts;
  totalRevenueOptions: any;
  operatingIncomeOptions: any;
  netIncomeFromContinuingOpsOptions: any;
  netIncomesOptions: any;

  options: any

  constructor(private securitiesService: SecuritiesService) { }

  public onChartInit(echarts: ECharts): void {
    echarts.resize({height:250, silent:true})
  }

  ngOnInit() {
    if (this.ticker) {

      this.securitiesService.getSecurityIncomeHistory(this.ticker).then(incomeStatementHistories => {
        let dates = [];
        let totalRevenues = [];
        let operatingIncomes = [];
        let netIncomeFromContinuingOps = [];
        let netIncomes = [];

        for (let i = incomeStatementHistories.length - 1; i >= 0; i--) {
          let incomeStatementHistory = incomeStatementHistories[i];

          dates.push(incomeStatementHistory.endDate);
          totalRevenues.push(incomeStatementHistory.totalRevenue);
          operatingIncomes.push(incomeStatementHistory.operatingIncome);
          netIncomeFromContinuingOps.push(incomeStatementHistory.netIncomeFromContinuingOps);
          netIncomes.push(incomeStatementHistory.netIncome);
        }

        this.totalRevenueOptions = {
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'shadow'
            }
          },
          xAxis: {
            type: 'category',
            data: dates
          },
          yAxis: {
            type: 'value'
          },
          series: [{
            data: totalRevenues,
            type: 'bar',
            showBackground: true,
            backgroundStyle: {
              color: 'rgba(180, 180, 180, 0.2)'
            }
          }]
        };

        this.operatingIncomeOptions = {
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'shadow'
            }
          },
          xAxis: {
            type: 'category',
            data: dates
          },
          yAxis: {
            type: 'value'
          },
          series: [{
            data: operatingIncomes,
            type: 'bar',
            showBackground: true,
            backgroundStyle: {
              color: 'rgba(180, 180, 180, 0.2)'
            }
          }]
        };

        this.netIncomeFromContinuingOpsOptions = {
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'shadow'
            }
          },
          xAxis: {
            type: 'category',
            data: dates
          },
          yAxis: {
            type: 'value'
          },
          series: [{
            data: netIncomeFromContinuingOps,
            type: 'bar',
            showBackground: true,
            backgroundStyle: {
              color: 'rgba(180, 180, 180, 0.2)'
            }
          }]
        };

        this.netIncomesOptions = {
          tooltip: {
            trigger: 'axis',
            axisPointer: {
              type: 'shadow'
            }
          },
          xAxis: {
            type: 'category',
            data: dates
          },
          yAxis: {
            type: 'value'
          },
          series: [{
            data: netIncomes,
            type: 'bar',
            showBackground: true,
            backgroundStyle: {
              color: 'rgba(180, 180, 180, 0.2)'
            }
          }]
        };

      }).catch(error => {
        console.error(error);
      });

      this.securitiesService.getSecurityBalanceSheetHistory(this.ticker).then(balanceSheetHistory => {
        // for (let i = 0; i < balanceSheetHistory.length; i++) {
        //   console.log(JSON.stringify(balanceSheetHistory[i]));
        // }
      });

      this.securitiesService.getSecurityCashflowStatementHistory(this.ticker).then(cashflowStatementHistory => {
        // for (let i = 0; i < cashflowStatementHistory.length; i++) {
        //   console.log(JSON.stringify(cashflowStatementHistory[i]));
        // }
      });
    }
  }
}
