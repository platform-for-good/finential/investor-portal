import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SocketioService } from 'src/app/lambda/socketio.service';

@Component({
  selector: 'app-technical-analysis',
  templateUrl: './technical-analysis.component.html',
  styleUrls: ['./technical-analysis.component.scss']
})
export class TechnicalAnalysisComponent implements OnInit {

  @Input() ticker: string;

  supportResistanceFormGroup: FormGroup;
  supportResistance: {
    name: string,
    period: number
  }

  trendChannelFormGroup: FormGroup;
  trendChannel: {
    name: string,
    period: number
  }

  rsiFormGroup: FormGroup;
  rsi: {
    name: string,
    period: number,
    overBought: number,
    overSold: number
  }

  pivotPointFormGroup: FormGroup;
  pivotPoint: {
    name: string,
    period: number
  }

  constructor(private socketioService: SocketioService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.supportResistance = {
      name: 'support_resistance',
      period: 60
    }

    this.supportResistanceFormGroup = this.formBuilder.group({
      period: [this.supportResistance.period, Validators.required]
    });

    this.trendChannel = {
      name: 'trend',
      period: 60
    }

    this.trendChannelFormGroup = this.formBuilder.group({
      period: [this.trendChannel.period, Validators.required]
    });

    this.pivotPoint = {
      name: 'pivot_point',
      period: 60
    }

    this.pivotPointFormGroup = this.formBuilder.group({
      period: [this.pivotPoint.period, Validators.required]
    });

    this.rsi = {
      name: 'rsi',
      period: 14,
      overBought: 80,
      overSold: 20
    }

    this.rsiFormGroup = this.formBuilder.group({
      period: [this.rsi.period, Validators.required],
      overBought: [this.rsi.overBought, Validators.required],
      overSold: [this.rsi.overSold, Validators.required]
    });
  }

  analyze(job: string) {
    let args = [];
    args.push(this.ticker);

    switch (job) {
      case 'pivot_point':
        args.push(-this.pivotPointFormGroup.value.period);
        break;
      case 'rsi':
        args.push(this.rsiFormGroup.value.period);
        args.push(this.rsiFormGroup.value.overBought);
        args.push(this.rsiFormGroup.value.overSold);
        break;
      case 'support_resistance':
        args.push(-this.supportResistanceFormGroup.value.period);
        break;
      case 'trend':
        args.push(-this.trendChannelFormGroup.value.period);
        break;
    }

    let id = this.socketioService.runSparkJob(job, args);
    console.log("Id " + id);
  }

}
