import { PortfolioPageComponent } from './portfolio-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: PortfolioPageComponent,
    data: { shouldReuse: true, key: 'portfolio' }
  },];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortfolioPageRoutingModule { }
