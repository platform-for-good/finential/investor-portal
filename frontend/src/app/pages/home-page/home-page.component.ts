import { AfterViewInit, Component, OnInit } from '@angular/core';
import { SocketioService } from 'src/app/lambda/socketio.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, AfterViewInit {

  gridColumns = 4;

  public personas: {
    name: string,
    ageGroup: string,
    phase: string,
    valueSought: string,
    type: string,
    image: string
  }[];

  constructor(public socketioService: SocketioService) {
    this.socketioService.setupSocketConnection();
  }

  ngOnInit() {
    this.personas = [
      {
        name: 'Socialite Alice',
        ageGroup: '15 - 25 yrs',
        phase: 'Beginner Student',
        type: 'Analyst',
        valueSought: 'Ability to start rewarding career with the earning capacity to live life as per their aspirations',
        image: 'assets/persona-1.png'
      },
      {
        name: 'Expert James',
        ageGroup: '25-33 yrs',
        phase: 'Early Career',
        type: 'Trader',
        valueSought: 'Recognition and ability to work on leading edge challenges with teams to find innovative solutions',
        image: 'assets/persona-2.png'
      },
      {
        name: 'Skeptic Aaron',
        ageGroup: '33-42 yrs',
        phase: 'Mid Career',
        type: 'Active Investor',
        valueSought: 'Tap on resources to identify opportunities efficiently and learn new investment strategies',
        image: 'assets/persona-3.png'
      },
      {
        name: 'Retiring Helen',
        ageGroup: '43-55 yrs',
        phase: 'Conservative',
        type: 'Investor',
        valueSought: 'Conservative Investment strategy to prioritize preservation of capital over market returns',
        image: 'assets/persona-4.png'
      }
    ]
  }

  ngAfterViewInit() {
  }
}
