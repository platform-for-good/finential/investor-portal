import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, timer } from 'rxjs';
// import { switchMap } from 'rxjs/operators';
import { SecuritiesService } from 'src/app/lambda/securities.service';
import { SocketioService } from 'src/app/lambda/socketio.service';
// import { WebsocketService } from 'src/app/lambda/websocket.service';
import { Security } from 'src/app/models/security';
import { Stock } from 'src/app/models/stock';
// import { environment } from '../../../environments/environment';

// export const REFRESH_INTERVAL = environment.refreshInterval;

@Component({
  selector: 'app-watchlist-page',
  templateUrl: './watchlist-page.component.html',
  styleUrls: ['./watchlist-page.component.scss']
})
export class WatchlistPageComponent implements OnInit, OnDestroy {

  gridColumns = 5;
  public data: {
    name: string,
    ticker: string,
    sector: string,
    industry: string,
    website: string,
    info?: Stock
  }[];

  subscription: Subscription;

  constructor(private securitiesService: SecuritiesService, /*private webSocketService: WebsocketService,*/ private socketioService: SocketioService) {
    this.data = [];

    this.socketioService.setupSocketConnection();
    this.subscription = this.socketioService.stock.subscribe(payload => {
      // console.log(payload);
      if (payload && this.data.length > 0) {
        this.data.find(d => d.ticker === payload.ticker).info = payload;
      }
    });

    // this.subscription = timer(0, REFRESH_INTERVAL)
    //   .pipe(switchMap(() =>
    //     this.webSocketService.dataUpdates$())).subscribe(result => {
    //       this.data.find(d => d.ticker === result.ticker).info = result;
    //     });
  }

  ngOnInit() {
    this.securitiesService.getAllSecuritiesInfo().then(securities => {

      for (let i = 0; i < securities.length; i++) {

        const security: Security = securities[i];

        this.securitiesService.getSecurityProfile(security.ticker).then(profile => {

          this.data.push({
            name: security.name,
            ticker: security.ticker,
            sector: profile.sector,
            industry: profile.industry,
            website: profile.website,
          });

        });
      }
    });
  }

  ngOnDestroy() {
    // this.webSocketService.closeConnection();
    this.subscription.unsubscribe();
  }
}
