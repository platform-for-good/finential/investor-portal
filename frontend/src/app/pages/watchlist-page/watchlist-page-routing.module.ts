import { WatchlistPageComponent } from './watchlist-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: WatchlistPageComponent,
    data:
    {
      shouldReuse: true,
      key: 'watchlist'
    }
  },
  {
    path: 'profile', 
    loadChildren: () =>
      import('../profile-page/profile-page.module').then(
        m => m.ProfilePageModule,
      ),
      data: {
        title: 'Profile',
        isChild: true
      }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WatchlistPageRoutingModule { }
