export interface Stock {
    name: string;
    ticker: string;
    date: string;
    preMarketChangePercent: number;
    preMarketChange: number;
    preMarketPrice: number;
    regularMarketPrice: number;
    regularMarketChange: number;
    regularMarketChangePercent: number;
    postMarketChangePercent:number;
    postMarketPrice:number;
    postMarketChange: number;
    currency: string;
    exchangeName: string;
    preMarket: boolean;
    regularMarket: boolean;
    postMarket: boolean;
    regularMarketPreviousClose: number;
    regularMarketDayHigh: number;
    regularMarketDayLow: number;
    regularMarketVolume: number;
    marketCap: number;
    image?: string;
}
