export interface FinancialData {
    id?: number,
    targetHighPrice: number,
    targetLowPrice: number,
    targetMeanPrice: number,
    recommendationKey: string,
    totalCash: number,
    totalCashPerShare: number,
    ebitda: number,
    totalDebt: number,
    quickRatio: number, // solvency
    currentRatio: number, // solvency
    totalRevenue: number,
    debtToEquity: number,
    revenuePerShare: number,
    returnOnAssets: number, // profitability
    returnOnEquity: number, // profitability
    grossProfits: number,
    freeCashflow: number,
    operatingCashflow: number,
    earningsGrowth: number, // growth rate
    revenueGrowth: number, // growth rate
    grossMargins: number,
    ebitdaMargins: number,
    operatingMargins: number,
    profitMargins: number,
    financialCurrency: string
}
