export interface CashflowStatement {
    id?: number,
    endDate: Date;
    netIncome: number;
    depreciation: number;
    changeToNetincome: number;
    changeToAccountReceivables: number;
    changeToLiabilities: number;
    changeToInventory: number;
    changeToOperatingActivities: number;
    totalCashFromOperatingActivities: number;
    capitalExpenditures: number;
    investments: number;
    otherCashflowsFromInvestingActivities: number;
    totalCashflowsFromInvestingActivities: number;
    dividendsPaid: number;
    netBorrowings: number;
    otherCashflowsFromFinancingActivities: number;
    totalCashFromFinancingActivities: number;
    effectOfExchangeRate: number,
    changeInCash: number;
    repurchaseOfStock: number;
    issuanceOfStock: number;
}
