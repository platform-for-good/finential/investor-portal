export interface Security {
    id?: number,
    name: string,
    ticker: string
}
