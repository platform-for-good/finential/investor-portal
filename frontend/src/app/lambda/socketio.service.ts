import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { io } from 'socket.io-client';
import { environment } from '../../environments/environment';
import { Stock } from '../models/stock';

export const SOCKET_ENDPOINT = environment.socketEndpoint;

@Injectable({
  providedIn: 'root'
})
export class SocketioService {

  stock: BehaviorSubject<Stock> = new BehaviorSubject<Stock>(null);
  socket;
  constructor() { }

  setupSocketConnection() {
    // if (!this.connected)
    this.socket = io(SOCKET_ENDPOINT);

    // this.socket.emit('my message', 'Hello there from Angular.');

    this.socket.on('stock-info', (data: string) => {
      this.stock.next(JSON.parse(data));

    }).on('spark-job-completed', (data: string) => {
      console.log(JSON.parse(data));

    }).on('connected', (connected: boolean) => {
      console.log(connected);

    });
  }

  public runSparkJob(job: string, args?: any[]): number {
    let id = Date.now();
    this.socket.emit('run-spark', JSON.stringify({ id, job, args }));

    return id;
  }

  refreshStockInfo() {
    this.stock.asObservable();
  }
}
