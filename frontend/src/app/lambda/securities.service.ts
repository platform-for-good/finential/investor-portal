import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// import * as csv from 'csvtojson';
// import * as moment from 'moment';

import { CrudService } from '../core/services/http/crud.service';
import { Security } from '../models/security';
import { Profile } from '../models/profile';
import { IncomeStatement } from '../models/income-statement';
import { BalanceSheet } from '../models/balance-sheet';
import { CashflowStatement } from '../models/cashflow-statement';
import { FinancialData } from '../models/financial-data';
import { environment } from '../../environments/environment';

export const RAW_DOWNLOAD_ENDPOINT = environment.rawDownloadEndpoint;
export const FEATURE_DOWNLOAD_ENDPOINT = environment.featureDownloadEndpoint;
export const RESTFUL_ENDPOINT = environment.restfulEndpoint;

@Injectable({
  providedIn: 'root'
})
export class SecuritiesService extends CrudService {

  endpoint: string;

  constructor(http: HttpClient) {
    super(http);
    this.url = RESTFUL_ENDPOINT;
  }

  public async getAllSecuritiesInfo(): Promise<Security[]> {

    const payload: Security[] = [];

    try {
      this.endpoint = 'securities';

      const response =
        await this.get<
          {
            id: number,
            name: string,
            ticker: string
          }[]>();

      // console.log("getAllSecurities(): " + JSON.stringify(response, null, 2));

      if (response && response.length > 0) {
        for (let i = 0; i < response.length; i++) {
          const item = response[i];

          const security: Security = {
            id: item.id,
            name: item.name,
            ticker: item.ticker
          };

          payload.push(security);
        }
      }

    } catch (e) {
      return Promise.reject(e.message);
    }

    return payload;
  }

  public async getAllSecurityPrice(ticker: string): Promise<any> {

    const payload = [];

    try {
      const response = await this.downloadFile(RAW_DOWNLOAD_ENDPOINT + '/securities/1d/' + ticker + '.csv');
      const value: string = await response.text();
      const tokens: string[] = value.split('\n');

      for (let i = 1; i < tokens.length; i++) {
        const currentTokens = tokens[i].split(',');
        const row = [];
        // let index = 0;
        row.push(currentTokens[0]); // date

        row.push(parseFloat(currentTokens[1])); // Open
        row.push(parseFloat(currentTokens[4])); // Close
        // row.push(parseFloat(currentTokens[5])); //Adj Close
        row.push(parseFloat(currentTokens[3])); // Low
        row.push(parseFloat(currentTokens[2])); // High        
        row.push(parseInt(currentTokens[6])); // Volume

        payload.push(row);
      }

    } catch (e) {
      return Promise.reject(e.message);
    }

    return payload;
  }

  public async getSecurityProfile(ticker: string): Promise<Profile> {
    let payload: Profile;

    try {
      const response = await this.downloadFile(RAW_DOWNLOAD_ENDPOINT + '/profile/' + ticker + '.json');
      const value = await response.text();
      const result = JSON.parse(value);
      const assetProfile = result.quoteSummary.result[0].assetProfile;

      payload = {
        address: assetProfile.address1,
        city: assetProfile.city,
        state: assetProfile.state,
        zip: assetProfile.zip,
        country: assetProfile.country,
        phone: assetProfile.phone,
        website: assetProfile.website,
        industry: assetProfile.industry,
        sector: assetProfile.sector,
        longBusinessSummary: assetProfile.longBusinessSummary,
        fullTimeEmployees: assetProfile.fullTimeEmployees
      }

    } catch (e) {
      return Promise.reject(e.message);
    }

    return payload;
  }

  public async getSecurityIncomeHistory(ticker: string): Promise<IncomeStatement[]> {
    const payload: IncomeStatement[] = [];

    try {

      const response = await this.downloadFile(RAW_DOWNLOAD_ENDPOINT + '/financial/' + ticker + '_income.json');
      const value = await response.text();
      const result = JSON.parse(value);
      const incomeStatementHistories = result.quoteSummary.result[0].incomeStatementHistory.incomeStatementHistory;

      for (let i = 0; i < incomeStatementHistories.length; i++) {
        const incomeStatementHistory = incomeStatementHistories[i];
        payload.push({
          endDate: incomeStatementHistory.endDate.fmt,
          totalRevenue: incomeStatementHistory.totalRevenue ? incomeStatementHistory.totalRevenue.raw : 0,
          costOfRevenue: incomeStatementHistory.costOfRevenue ? incomeStatementHistory.costOfRevenue.raw : 0,
          grossProfit: incomeStatementHistory.grossProfit ? incomeStatementHistory.grossProfit.raw : 0,
          researchDevelopment: incomeStatementHistory.researchDevelopment ? incomeStatementHistory.researchDevelopment.raw : 0,
          sellingGeneralAdministrative: incomeStatementHistory.sellingGeneralAdministrative ? incomeStatementHistory.sellingGeneralAdministrative.raw : 0,
          nonRecurring: incomeStatementHistory.nonRecurring ? incomeStatementHistory.nonRecurring.raw : 0,
          otherOperatingExpenses: incomeStatementHistory.otherOperatingExpenses ? incomeStatementHistory.otherOperatingExpenses.raw : 0,
          totalOperatingExpenses: incomeStatementHistory.totalOperatingExpenses ? incomeStatementHistory.totalOperatingExpenses.raw : 0,
          operatingIncome: incomeStatementHistory.operatingIncome ? incomeStatementHistory.operatingIncome.raw : 0,
          totalOtherIncomeExpenseNet: incomeStatementHistory.totalOtherIncomeExpenseNet ? incomeStatementHistory.totalOtherIncomeExpenseNet.raw : 0,
          ebit: incomeStatementHistory.ebit ? incomeStatementHistory.ebit.raw : 0,
          interestExpense: incomeStatementHistory.interestExpense ? incomeStatementHistory.interestExpense.raw : 0,
          incomeBeforeTax: incomeStatementHistory.incomeBeforeTax ? incomeStatementHistory.incomeBeforeTax.raw : 0,
          incomeTaxExpense: incomeStatementHistory.incomeTaxExpense ? incomeStatementHistory.incomeTaxExpense.raw : 0,
          minorityInterest: incomeStatementHistory.minorityInterest ? incomeStatementHistory.minorityInterest.raw : 0,
          netIncomeFromContinuingOps: incomeStatementHistory.netIncomeFromContinuingOps ? incomeStatementHistory.netIncomeFromContinuingOps.raw : 0,
          discontinuedOperations: incomeStatementHistory.discontinuedOperations ? incomeStatementHistory.discontinuedOperations.raw : 0,
          extraordinaryItems: incomeStatementHistory.extraordinaryItems ? incomeStatementHistory.extraordinaryItems.raw : 0,
          effectOfAccountingCharges: incomeStatementHistory.effectOfAccountingCharges ? incomeStatementHistory.effectOfAccountingCharges.raw : 0,
          otherItems: incomeStatementHistory.otherItems ? incomeStatementHistory.otherItems.raw : 0,
          netIncome: incomeStatementHistory.netIncome ? incomeStatementHistory.netIncome.raw : 0,
          netIncomeApplicableToCommonShares: incomeStatementHistory.netIncomeApplicableToCommonShares ? incomeStatementHistory.netIncomeApplicableToCommonShares.raw : 0
        });
      }

    } catch (e) {
      return Promise.reject(e.message);
    }

    return payload;
  }

  public async getSecurityBalanceSheetHistory(ticker: string): Promise<BalanceSheet[]> {
    const payload: BalanceSheet[] = [];

    try {

      const response = await this.downloadFile(RAW_DOWNLOAD_ENDPOINT + '/financial/' + ticker + '_balance.json');
      const value = await response.text();
      const result = JSON.parse(value);
      const balanceSheetStatements = result.quoteSummary.result[0].balanceSheetHistory.balanceSheetStatements;

      for (let i = 0; i < balanceSheetStatements.length; i++) {
        const balanceSheetStatement = balanceSheetStatements[i];
        payload.push({
          endDate: balanceSheetStatement.endDate.fmt,
          cash: balanceSheetStatement.cash ? balanceSheetStatement.cash.raw : 0,
          shortTermInvestments: balanceSheetStatement.shortTermInvestments ? balanceSheetStatement.shortTermInvestments.raw : 0,
          netReceivables: balanceSheetStatement.netReceivables ? balanceSheetStatement.netReceivables.raw : 0,
          inventory: balanceSheetStatement.inventory ? balanceSheetStatement.inventory.raw : 0,
          otherCurrentAssets: balanceSheetStatement.otherCurrentAssets ? balanceSheetStatement.otherCurrentAssets.raw : 0,
          totalCurrentAssets: balanceSheetStatement.totalCurrentAssets ? balanceSheetStatement.totalCurrentAssets.raw : 0,
          longTermInvestments: balanceSheetStatement.longTermInvestments ? balanceSheetStatement.longTermInvestments.raw : 0,
          propertyPlantEquipment: balanceSheetStatement.propertyPlantEquipment ? balanceSheetStatement.propertyPlantEquipment.raw : 0,
          goodWill: balanceSheetStatement.goodWill ? balanceSheetStatement.goodWill.raw : 0,
          intangibleAssets: balanceSheetStatement.intangibleAssets ? balanceSheetStatement.intangibleAssets.raw : 0,
          otherAssets: balanceSheetStatement.otherAssets ? balanceSheetStatement.otherAssets.raw : 0,
          totalAssets: balanceSheetStatement.totalAssets ? balanceSheetStatement.totalAssets.raw : 0,
          accountsPayable: balanceSheetStatement.accountsPayable ? balanceSheetStatement.accountsPayable.raw : 0,
          shortLongTermDebt: balanceSheetStatement.shortLongTermDebt ? balanceSheetStatement.shortLongTermDebt.raw : 0,
          otherCurrentLiab: balanceSheetStatement.otherCurrentLiab ? balanceSheetStatement.otherCurrentLiab.raw : 0,
          longTermDebt: balanceSheetStatement.longTermDebt ? balanceSheetStatement.longTermDebt.raw : 0,
          otherLiab: balanceSheetStatement.otherLiab ? balanceSheetStatement.otherLiab.raw : 0,
          totalCurrentLiabilities: balanceSheetStatement.totalCurrentLiabilities ? balanceSheetStatement.totalCurrentLiabilities.raw : 0,
          totalLiab: balanceSheetStatement.totalLiab ? balanceSheetStatement.totalLiab.raw : 0,
          commonStock: balanceSheetStatement.commonStock ? balanceSheetStatement.commonStock.raw : 0,
          retainedEarnings: balanceSheetStatement.retainedEarnings ? balanceSheetStatement.retainedEarnings.raw : 0,
          treasuryStock: balanceSheetStatement.treasuryStock ? balanceSheetStatement.treasuryStock.raw : 0,
          capitalSurplus: balanceSheetStatement.capitalSurplus ? balanceSheetStatement.capitalSurplus.raw : 0,
          otherStockholderEquity: balanceSheetStatement.otherStockholderEquity ? balanceSheetStatement.otherStockholderEquity.raw : 0,
          totalStockholderEquity: balanceSheetStatement.totalStockholderEquity ? balanceSheetStatement.totalStockholderEquity.raw : 0,
          netTangibleAssets: balanceSheetStatement.netTangibleAssets ? balanceSheetStatement.netTangibleAssets.raw : 0
        });
      }

    } catch (e) {
      return Promise.reject(e.message);
    }

    return payload;
  }

  public async getSecurityCashflowStatementHistory(ticker: string): Promise<CashflowStatement[]> {
    const payload: CashflowStatement[] = [];

    try {

      const response = await this.downloadFile(RAW_DOWNLOAD_ENDPOINT + '/financial/' + ticker + '_cashflow.json');
      const value = await response.text();
      const result = JSON.parse(value);
      const cashflowStatements = result.quoteSummary.result[0].cashflowStatementHistory.cashflowStatements;

      for (let i = 0; i < cashflowStatements.length; i++) {
        const cashflowStatement = cashflowStatements[i];
        payload.push({
          endDate: cashflowStatement.endDate.fmt,
          netIncome: cashflowStatement.netIncome ? cashflowStatement.netIncome.raw : 0,
          depreciation: cashflowStatement.depreciation ? cashflowStatement.depreciation.raw : 0,
          changeToNetincome: cashflowStatement.changeToNetincome ? cashflowStatement.changeToNetincome.raw : 0,
          changeToAccountReceivables: cashflowStatement.changeToAccountReceivables ? cashflowStatement.changeToAccountReceivables.raw : 0,
          changeToLiabilities: cashflowStatement.changeToLiabilities ? cashflowStatement.changeToLiabilities.raw : 0,
          changeToInventory: cashflowStatement.changeToInventory ? cashflowStatement.changeToInventory.raw : 0,
          changeToOperatingActivities: cashflowStatement.changeToOperatingActivities ? cashflowStatement.changeToOperatingActivities.raw : 0,
          totalCashFromOperatingActivities: cashflowStatement.totalCashFromOperatingActivities ? cashflowStatement.totalCashFromOperatingActivities.raw : 0,
          capitalExpenditures: cashflowStatement.capitalExpenditures ? cashflowStatement.capitalExpenditures.raw : 0,
          investments: cashflowStatement.investments ? cashflowStatement.investments.raw : 0,
          otherCashflowsFromInvestingActivities: cashflowStatement.otherCashflowsFromInvestingActivities ? cashflowStatement.otherCashflowsFromInvestingActivities.raw : 0,
          totalCashflowsFromInvestingActivities: cashflowStatement.totalCashflowsFromInvestingActivities ? cashflowStatement.totalCashflowsFromInvestingActivities.raw : 0,
          dividendsPaid: cashflowStatement.dividendsPaid ? cashflowStatement.dividendsPaid.raw : 0,
          netBorrowings: cashflowStatement.netBorrowings ? cashflowStatement.netBorrowings.raw : 0,
          otherCashflowsFromFinancingActivities: cashflowStatement.otherCashflowsFromFinancingActivities ? cashflowStatement.otherCashflowsFromFinancingActivities.raw : 0,
          totalCashFromFinancingActivities: cashflowStatement.totalCashFromFinancingActivities ? cashflowStatement.totalCashFromFinancingActivities.raw : 0,
          effectOfExchangeRate: cashflowStatement.effectOfExchangeRate ? cashflowStatement.effectOfExchangeRate.raw : 0,
          changeInCash: cashflowStatement.changeInCash ? cashflowStatement.changeInCash.raw : 0,
          repurchaseOfStock: cashflowStatement.repurchaseOfStock ? cashflowStatement.repurchaseOfStock.raw : 0,
          issuanceOfStock: cashflowStatement.issuanceOfStock ? cashflowStatement.issuanceOfStock.raw : 0
        });
      }

    } catch (e) {
      return Promise.reject(e.message);
    }

    return payload;
  }

  public async getSecurityFinancialData(ticker: string): Promise<FinancialData> {
    let payload: FinancialData;

    try {

      const response = await this.downloadFile(RAW_DOWNLOAD_ENDPOINT + '/financial/' + ticker + '_financial.json');
      const value = await response.text();
      const result = JSON.parse(value);
      const financialData = result.quoteSummary.result[0].financialData;

      payload = {
        targetHighPrice: financialData.targetHighPrice.raw,
        targetLowPrice: financialData.targetLowPrice.raw,
        targetMeanPrice: financialData.targetMeanPrice.raw,
        recommendationKey: financialData.recommendationKey,
        totalCash: financialData.totalCash.raw,
        totalCashPerShare: financialData.totalCashPerShare.raw,
        ebitda: financialData.ebitda.raw,
        totalDebt: financialData.totalDebt.raw,
        quickRatio: financialData.quickRatio.raw, // solvency
        currentRatio: financialData.currentRatio.raw, // solvency
        totalRevenue: financialData.totalRevenue.raw,
        debtToEquity: financialData.debtToEquity.raw,
        revenuePerShare: financialData.revenuePerShare.raw,
        returnOnAssets: financialData.returnOnAssets.raw, // profitability
        returnOnEquity: financialData.returnOnEquity.raw, // profitability
        grossProfits: financialData.grossProfits.raw,
        freeCashflow: financialData.freeCashflow.raw,
        operatingCashflow: financialData.operatingCashflow.raw,
        earningsGrowth: financialData.earningsGrowth.raw, // growth rate
        revenueGrowth: financialData.revenueGrowth.raw, // growth rate
        grossMargins: financialData.grossMargins.raw,
        ebitdaMargins: financialData.ebitdaMargins.raw,
        operatingMargins: financialData.operatingMargins.raw,
        profitMargins: financialData.profitMargins.raw,
        financialCurrency: financialData.financialCurrency
      }

    } catch (e) {
      return Promise.reject(e.message);
    }

    return payload;
  }
}
