import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

export abstract class CrudService<T = any> {
    abstract endpoint;
    headers: HttpHeaders;
    url = '';

    protected constructor(protected http: HttpClient) {
        this.url = '';
        this.headers = new HttpHeaders();
        this.headers.set('Content-Type', 'application/json');
    }

    public async get<G>(request?: string): Promise<G | null> {
        let response = null;
        try {

            if (request) {

                // console.log(`${this.url}/${this.endpoint}/${request}`);
                response = await this.http
                    .get<G>(`${this.url}/${this.endpoint}/${request}`, { headers: this.headers })
                    .toPromise();

            } else {

                // console.log(`${this.url}/${this.endpoint}`);
                response = await this.http
                    .get<G>(`${this.url}/${this.endpoint}`, { headers: this.headers })
                    .toPromise();
            }
        } catch (error) {
            response = this.errorHandler('GET', error);
        }

        return response;
    }

    public async downloadFile(fileName: string): Promise<any> {

        let response = null;
        try {
            response = await this.http.get(`${this.url}/${fileName}`, { responseType: 'blob' }).toPromise();
        } catch (error) {
            response = this.errorHandler('GET', error);
        }

        return response;
    }

    public async getList(): Promise<T[] | null> {
        return this.get<T[]>('list');
    }

    public async getById(id: number | string): Promise<T | null> {
        return this.get<T>('' + id);
    }

    public async post(body): Promise<any> {
        let response = null;
        try {
            response = await this.http
                .post(`${this.url}/${this.endpoint}`, body)
                .toPromise();
        } catch (error) {
            response = this.errorHandler('POST', error);
        }
        return response;
    }

    public async deleteById(id: number | string): Promise<any> {
        let response = null;
        try {
            response = await this.http
                .delete(`${this.url}/${this.endpoint}/${id}`)
                .toPromise();
        } catch (error) {
            response = this.errorHandler('DELETE', error);
        }
        return response;
    }

    public errorHandler(
        method: string,
        error: HttpErrorResponse,
    ): Promise<never> {
        console.error(
            `Error occurred during ${method} ${this.url}/${this.endpoint}`,
            error,
        );
        return Promise.reject(error);
    }
}
